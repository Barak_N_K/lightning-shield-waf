#All the variables and values below are constants to be used in the program.
HOST = "127.0.0.1"
PORT = 8000
SOCKET_TIMEOUT = 25
SQLITE_PATH = "sqlite:///database.db?check_same_thread=False"
SQL_WHITLIST = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.@#<>/ " #These characters are allowed
INJECTION_DETECTING_REGEX = "'(''|[^'])*'"
EMAIL_VALIDATE_REGEX = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
FIREWALL_RULE_NAME = "Lightning Shield - WAF"
MAX_CONNECTIONS_PER_IP = 25 #There is usually no point in having more than 25 connections per IP. Too many connections may cause a DoS.
LOGGING_FILENAME = "logs.txt"